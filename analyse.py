#!/usr/bin/env python3

import pprint
#import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math
import os
import pandas as pd

from scipy.optimize import curve_fit

from ROOT import TFile

__author__ = "Josh McFayden"
__doc__ = """Script to extract EFT sensitivity and make pretty plots"""


def fitfunc(x,a,b,c):
    return a*x*x + b*x + c

def getDict(basedir,infilename):
    d={}
    infile=open(basedir+"/"+infilename)
    lines=infile.readlines()
    for line in lines:
        title=r''
        vals=line.strip().split(';')

        key=vals[0]
        if key in d:
            print(f"ERROR: Key \"{key}\" already found in dictionary")

        title=vals[1] if len(vals) > 1 else None
        limit=vals[2] if len(vals) > 2 else None
        
        if not limit:
            d[key]={"title":title}
        else:
            d[key]={"title":title,"limit":float(limit)}
            
    return d



# Parse options from command line
from optparse import OptionParser
usage = "usage: %prog [options] <base directory>"
parser = OptionParser(usage=usage)#, version="%prog")
parser.add_option("-v", "--verbose", action="store_true", dest="verbose",    help="Set verbose mode (default: %default)")
parser.add_option("-q", "--quiet", action="store_true", dest="quiet",    help="Set quiet mode (default: %default)")
parser.add_option("-s", "--signals", action="store", dest="signalsfilename", help="File name of signals text file (default: %default)")
parser.add_option("-b", "--backgrounds", action="store", dest="bkgsfilename", help="File name of backgrounds text file (default: %default)")
parser.add_option("-l", "--limits", action="store", dest="oplimitsfilename", help="File name of operator limits text file (default: %default)")
parser.add_option("-i", "--inputs", action="store", dest="inputsdir", help="Directory to find input signals, backgrounds and operator limits text files (default: %default)")

parser.set_defaults(verbose=False,quiet=False,signalsfilename="signals.txt",bkgsfilename="backgrounds.txt",oplimitsfilename="operator_limits.txt",inputsdir=None)

(opts, args) = parser.parse_args()

if len(args) < 1:
    print(parser.error("Base directory argument required"))

if len(args) < 2:
    print(parser.error("Run name argument required"))
    
basedir=args[0]
runname=args[1]

inputsdir=opts.inputsdir if opts.inputsdir else basedir
    
#signals=getDict(basedir,"signals.txt")
#bkgs=getDict(basedir,"backgrounds.txt")
#efts=getDict(basedir,"operator_limits.txt")
signals=getDict(inputsdir,opts.signalsfilename)
bkgs=getDict(inputsdir,opts.bkgsfilename)
efts=getDict(inputsdir,opts.oplimitsfilename)

print("Found the following EFT operators and limits:")
print(efts)

if not os.path.isdir(f'{basedir}/EFT/EFTViz/'):
    os.mkdir(f'{basedir}/EFT/EFTViz/')
if not os.path.isdir(f'{basedir}/EFT/EFTViz/EGrowth/'):
    os.mkdir(f'{basedir}/EFT/EFTViz/EGrowth/')
if not os.path.isdir(f'{basedir}/EFT/EFTViz/SummaryTables/'):
    os.mkdir(f'{basedir}/EFT/EFTViz/SummaryTables/')
if not os.path.isdir(f'{basedir}/EFT/EFTViz/Heatmaps/'):
    os.mkdir(f'{basedir}/EFT/EFTViz/Heatmaps/')

def main():

    
    # Build dictionary containing all bin-based information
    
    b_dict={}

    for signal in signals:

        print("SIGNAL:",signal)
        
        l_EFT=[]
        terms=[]
        term_dict={}

        # Get EFT information 
        with open(f"{basedir}/EFT/EFT_Fit_results_{signal}.txt","r") as f_EFT:
            l_EFT = f_EFT.readlines()

        EFT_data=[x.split() for x in l_EFT]

                                            
        

        for EFT in EFT_data:
      
            if not len(EFT): continue

            # find eqn terms for coeffs
            if str(EFT[0]) == "NORMFACTORS":
                terms = EFT[1:]
                print("TERMS:",terms)

                for term in terms:
                    term_dict[term]={"coeff":None,
                                     "orders":[term.count(e) for e in efts]}
                if opts.verbose:
                    pprint.pprint(term_dict)
                
            # get coeffs
            else:
                # get bin
                b=EFT[0].replace(f'Expression_muEFT_{signal}_','')
                coeffs=EFT[1:]

                #add new bins to dict if needed
                if b not in b_dict:
                    b_dict[b]={}
                #add new signal bin dict if needed
                if signal not in b_dict[b]:
                    b_dict[b][signal]={}
                    for term in term_dict:
                        b_dict[b][signal][term]={"coeff":term_dict[term]["coeff"],
                                               "orders":term_dict[term]["orders"]}

                # fill dictionary with coeffs and orders etc.
                for iterm,term in enumerate(term_dict):
                    b_dict[b][signal][term]["coeff"]=coeffs[iterm]


    print("Found %i region bins"%(len(b_dict)))

    #plot_bar(b_dict,"EFT","process",signals,efts,stack=False,order=2,useyield=True,frac=False,eftlims=True,background="significance")
    #plot_bar(b_dict,"EFT","process",signals,efts,stack=False,order=1,useyield=True,frac=False,eftlims=True,background="significance")
    
    #plot_bar_2D(b_dict,"EFT","process",signals,efts,stack=False,order=1,eftlims=True,useyield=True,background="significance")
    #plot_bar_2D(b_dict,"process","EFT",signals,efts,stack=False,order=1,eftlims=True,useyield=True,background="significance")



    for o in [1,2]:
        plot_bar(b_dict,"EFT","process",signals,efts,stack=False,order=o,useyield=True,frac=False,eftlims=True,background="SB")
        plot_bar(b_dict,"EFT","process",signals,efts,stack=False,order=o,useyield=True,frac=False,eftlims=True,background="significance")
        plot_bar(b_dict,"process","EFT",signals,efts,stack=False,order=o,useyield=True,frac=False,eftlims=True,background="SB")
        plot_bar(b_dict,"process","EFT",signals,efts,stack=False,order=o,useyield=True,frac=False,eftlims=True,background="significance")
    
        plot_bar_2D(b_dict,"EFT","process",signals,efts,stack=False,order=o,eftlims=True)
        plot_bar_2D(b_dict,"EFT","process",signals,efts,stack=False,order=o,useyield=True,eftlims=True)
        plot_bar_2D(b_dict,"EFT","process",signals,efts,stack=True,frac=True,order=o,useyield=True,eftlims=True)
        plot_bar_2D(b_dict,"EFT","process",signals,efts,stack=False,order=o,eftlims=True,useyield=True,background="SB")
        plot_bar_2D(b_dict,"EFT","process",signals,efts,stack=False,order=o,eftlims=True,useyield=True,background="significance")
        
        plot_bar_2D(b_dict,"process","EFT",signals,efts,stack=False,order=o,eftlims=True)
        plot_bar_2D(b_dict,"process","EFT",signals,efts,stack=False,order=o,useyield=True,eftlims=True)
        plot_bar_2D(b_dict,"process","EFT",signals,efts,stack=True,frac=True,order=o,useyield=True,eftlims=True)
        plot_bar_2D(b_dict,"process","EFT",signals,efts,stack=False,order=o,eftlims=True,useyield=True,background="SB")
        plot_bar_2D(b_dict,"process","EFT",signals,efts,stack=False,order=o,eftlims=True,useyield=True,background="significance")

    plot_heatmap(b_dict,signals,efts,order=1,useyield=True,frac=False,eftlims=True,background="significance")


def plot_bar(b_dict,splitname,groupname,insignals,inefts,order=None,stack=True,useyield=False,frac=None,eftlims=False,background=False,suff=None):

    bins = [b for b in b_dict]

    # Insert gaps between regions
    width = 0.8
    if not stack:
        width=0.3

    xpos=[]
    icount=0
    old=None
    for b in b_dict:
        new=b.split('_bin')[0]
        #Just need this first time
        if not old:
            old=new

        if new != old:
            old=new
            icount+=1
            xpos.append(icount+width/2)
        else:
            xpos.append(icount+width/2)

        icount+=1


    p_split = {}
    p_split_rel = {}
    split=None

    ordername="unknown"
    if order==1: ordername="linear"
    if order==2: ordername="quadratic"
    
    
    if splitname=="process":
        split=insignals
        group=inefts
    else:
        split=inefts
        group=insignals
        
    totals=np.zeros(len(b_dict))
    ymin=0
    ymax=-1

    EFTType={}
    # Loop over split values
    for s in split:
        print("Split:",s)
        b_vals=[]
        b_vals_rel=[]

        # Loop over region bins
        for b in b_dict:
            if opts.verbose:
                print(b)

            nb=int(b.split('_bin')[1])
            
            res=0
            rel=0
            
            if not useyield:
                # Get the relative difference - i.e. just use the sigma_EFT/sigma_SM values from the EFT parametrisaton
                
                if splitname=="process":
                    eqn=geteqn(b_dict[b],insignals=[s],inorder=order)
                else:
                    eqn=geteqn(b_dict[b],inefts=[s],inorder=order)
                    
                # Evaluate equation at a given set of coeff values
                #res=eval(eqn,{k:v["limit"] for k,v in inefts.items()})
                #@todo Should really check largest correction for positive and negative...?!
                if eftlims:
                    res=eval(eqn,{k:v["limit"] for k,v in inefts.items()})
                else:
                    res=eval(eqn,{k:1.0 for k,v in inefts.items()})
                

                res=res-1.0
                rel=res
                
            else:
                # Get the absolute difference - i.e. sigma_EFT/sigma_SM multiplied by the SM yield
                
                region=b.split('_bin')[0]
                hyield=0
                if splitname=="process":

                    eqn=geteqn(b_dict[b],insignals=[s],inorder=order)
                    
                    #res=eval(eqn,{k:v["limit"] for k,v in inefts.items()})
                    if eftlims:
                        res=eval(eqn,{k:v["limit"] for k,v in inefts.items()})
                    else:
                        res=eval(eqn,{k:1.0 for k,v in inefts.items()})

                    res=res-1.0
                    rel=res
                    
                    filename=f"{basedir}/Histograms/{runname}_{region}_histos.root"
                    rfile=TFile.Open(filename)
                    histname=f"{region}/SM_{s}_{b}/nominal/{region}_SM_{s}_{b}"
                    hist=rfile.Get(histname)

                    if hist:
                        hyield=hist.Integral()
                    else:
                        print(f"WARNING: {histname} not found in {filename}")

                    if opts.verbose:
                        print(filename,histname,hyield,hyield*res)

                    res=hyield*res

                    if background:
                        sumbkg=0
                        if "bkg" in b_dict[b]:
                            sumbkg=b_dict[b]["bkg"]
                        else:
                            print("Finding background for",b)
                            for bkg in bkgs:
                                
                                histname=f"{region}/{bkg}/nominal/{region}_{bkg}"
                                hist=rfile.Get(histname)
                                if hist:
                                    hyield=hist.GetBinContent(nb+1)
                                    sumbkg+=hyield
                                    if opts.verbose:
                                        print("BKG:",bkg,nb+1,hyield,sumbkg)
                                else:
                                    print(f"WARNING: {histname} not found in {filename}")
                                    
                            for signal in signals:
                                
                                histname=f"{region}/{signal}/nominal/{region}_{signal}"
                                hist=rfile.Get(histname)
                                if hist:
                                    hyield=hist.GetBinContent(nb+1)
                                    sumbkg+=hyield
                                    if opts.verbose:
                                        print("BKG (SM):",signal,nb+1,hyield,sumbkg)
                                else:
                                    print(f"WARNING: {histname} not found in {filename}")

                            b_dict[b]["bkg"]=sumbkg

                        if background=="SB":
                            res=res/sumbkg
                        elif background=="significance":
                            res=res/math.sqrt(sumbkg)
                        elif background=="significancesyst":
                            res=res/math.sqrt(sumbkg+(0.3*sumbkg*0.3*sumbkg))
                            #@todo: get actual systematic uncertainty
                    
                else:
                    #if split is by EFT need to add up all the signal yields by hand
                    filename=f"{basedir}/Histograms/{runname}_{region}_histos.root"
                    rfile=TFile.Open(filename)
                    sig_tot=0.
                    for signal in signals:
                        eqn=geteqn(b_dict[b],insignals=[signal],inefts=[s],inorder=order)
                        #print("TEST",eqn,)
                        signalres=eval(eqn,{k:v["limit"] for k,v in inefts.items()})
                        signalres=signalres-1.0

                        
                        histname=f"{region}/SM_{signal}_{b}/nominal/{region}_SM_{signal}_{b}"
                        hist=rfile.Get(histname)
                        if hist:
                            hyield=hist.Integral()
                        else:
                            print(f"WARNING: {histname} not found in {filename}")
                        if opts.verbose:
                            print(filename,histname,hyield,hyield*signalres,res)

                        sig_tot+=hyield
                        res+=hyield*signalres    

                    rel=res/sig_tot if sig_tot else 0.
                    
                    if background:
                        sumbkg=0
                        if "bkg" in b_dict[b]:
                            sumbkg=b_dict[b]["bkg"]
                        else:
                            print("Finding background for",b)
                            for bkg in bkgs:
                            
                                histname=f"{region}/{bkg}/nominal/{region}_{bkg}"
                                hist=rfile.Get(histname)
                                if hist:
                                    hyield=hist.GetBinContent(nb+1)
                                    sumbkg+=hyield
                                    if opts.verbose:
                                        print("BKG:",bkg,nb+1,hyield,sumbkg)
                                else:
                                    print(f"WARNING: {histname} not found in {filename}")
                            
                            for signal in signals:
                            
                                histname=f"{region}/{signal}/nominal/{region}_{signal}"
                                hist=rfile.Get(histname)
                                if hist:
                                    hyield=hist.GetBinContent(nb+1)
                                    sumbkg+=hyield
                                    if opts.verbose:
                                        print("BKG (SM):",signal,nb+1,hyield,sumbkg)
                                else:
                                    print(f"WARNING: {histname} not found in {filename}")

                            b_dict[b]["bkg"]=sumbkg
                    
                        if background=="SB":
                            res=res/sumbkg
                        elif background=="significance":
                            res=res/math.sqrt(sumbkg)
                        elif background=="significancesyst":
                            res=res/math.sqrt(sumbkg+(0.3*sumbkg*0.3*sumbkg))

                           
                            
            b_vals.append(res)
            b_vals_rel.append(rel)
            
        if opts.verbose:
            print(b_vals)
        ymin=min(ymin,min(b_vals))
        ymax=max(ymax,max(b_vals))
        totals+=np.array(b_vals)
        p_split[s]=np.array(b_vals)
        p_split_rel[s]=np.array(b_vals_rel)

    if frac:
        for s in split:
            p_split[s]=np.divide(p_split[s],np.array(totals))

    # Do pruning
    if background=="significance":
        pruned=[]
        #reset
        for b in b_dict:
            b_dict[b]["significant"]=[]
        
        threshold=0.5

        for s in split:
            maxval=-999
            # find regions for energy growth test fit
            regions=[]
            
            for ib,b in enumerate(b_dict):
                region=b.split('_bin')[0]
                if region not in regions:
                    regions.append(region)
                    
                val=p_split[s][ib]
                #print(s,b,val)
                if abs(val)>threshold:
                    if "significant" in b_dict[b]:
                        b_dict[b]["significant"].append((s,val))
                    else:
                        b_dict[b]["significant"]=[(s,val)]
                        
                maxval=max(abs(val),maxval)
            if maxval < threshold:
                pruned.append(s)
                print(f'  --> Max val = {maxval}, {s} is pruned')
            else:
                # Test energy growth
                for r in regions:
                    x=[]
                    y=[]

                    for ib,b in enumerate(b_dict):
                        #print("DEBUG:",f'{r}_bin', b)
                        
                        if f'{r}_bin' in b:
                            nbin=b.split(f'{r}_bin')[1]
                            x.append(float(nbin))
                            vals=[x[1] for x in b_dict[b]["significant"] if x[0]==s]
                            if len(vals)>1:
                                print("ERROR: Too many operator values found!")
                            y.append(float(p_split_rel[s][ib]))
                            continue
                    
                    if len(x) < 3:
                        print(f'Skipping E-Growth test for region {r} - not enough bins ({len(x)})')
                        continue
                    
                    #print("DEBUG: x=",x)
                    #print("DEBUG: y=",y)
                    start_values=[0.,0.,x[0]]
                    x=np.array(x)
                    y=np.array(y)


                    errs=[0.05 for i in y]
                    fig, ax = plt.subplots(figsize = (11,11), facecolor = 'white', )
                    #ax.figure(
                    ax.errorbar(x,y,errs,c='black',marker='o',label='data')
                    plt.title("Region data")
                    plt.ylabel('Rel EFT variation')
                    plt.xlabel('bin')
                    #plt.ylim(0, 50.)
                    
                    p_opt, p_cov = curve_fit(fitfunc,x,y,sigma=errs,p0=start_values)
                    #print("DEBUG: p_opt:",p_opt)
                    textstr=""
                    EFTdep=None
                    optstr=",".join([f'{p:.3f}' for p in p_opt])
                    if abs(p_opt[0]) > 0.05 or abs(p_opt[1]) > 0.05:
                        #print("DEBUG: -> EFT Energy Growth")
                        EFTdep="EGrowth"
                        textstr="EFT Energy Growth\n"+optstr
                        b_dict[b]["significant"]=[(s,val)]
                    elif abs(p_opt[2]) > 0.05 :
                        #print("DEBUG: -> EFT Flat")
                        EFTdep="Flat"
                        textstr="EFT Flat\n"+optstr
                    else:
                        #print("DEBUG: -> No EFT")
                        EFTdep="NoEFT"
                        textstr="No EFT\n"+optstr

                    if r not in EFTType:
                        EFTType[r]={}
                    EFTType[r][s]=EFTdep
                    #print("DEBUG:",f'EFTType[{r}][{s}]={EFTdep}')
                    
                    #for b in b_dict:
                    #    if region in b:
                    #        for sig in b_dict[b]["significant"]:
                    #            if s==sig[0]:
                    #                b_dict[b]["significant"]:
                        
                    # place a text box in upper left in axes coords
                    plt.text(0.05, 0.95, textstr, transform=ax.transAxes,
                             fontsize=14,
                             verticalalignment='top')#, bbox=props)
                    
                    xfine=np.linspace(x[0],x[-1],100)
                    plt.plot(xfine,fitfunc(xfine,*p_opt),'r--', linewidth=2,label='fit')
                    plt.savefig(f'{basedir}/EFT/EFTViz/EGrowth/EGrowth_{r}_{s}.png')
                    plt.savefig(f'{basedir}/EFT/EFTViz/EGrowth/EGrowth_{r}_{s}.pdf')
                    
                        

        pruning_web=open(f'{basedir}/EFT/EFTViz/Pruning.html','w')
        print("Pruned values:",len(pruned),"-",pruned)
        pruning_web.write(f'<h3>Pruned values:</h3> {len(pruned)} - {pruned}')
        print("Significant values for each bin:")
        pruning_web.write('<h3>Significant values for each bin:</h3>')
        for b in b_dict:
            if "significant" in b_dict[b]:
                # Sort by abs value highest to lowest
                tmpsort=[abs(x[1]) for x in b_dict[b]["significant"]]
                tmpsort.sort(reverse=True)
                bssorted=[]
                for ts in tmpsort:
                    for bs in b_dict[b]["significant"]:
                        if abs(bs[1]) == ts:
                            bssorted.append(bs)
                            break
                b_dict[b]["significant"]=bssorted

                print(" -",b,"-",len(b_dict[b]["significant"]),":",[(bds[0],f'{bds[1]:.2f}',EFTType[b.split('_bin')[0]][bds[0]]) for bds in b_dict[b]["significant"]])
                pruning_web.write(f'<h4> - {b} - {len(b_dict[b]["significant"])} : </h4></br>')
                for bds in b_dict[b]["significant"]:
                    r=b.split('_bin')[0]
                    s=bds[0]
                    pstr=f'  - {bds[0]}, {bds[1]:.2f}, '+f'<a href="EGrowth/EGrowth_{r}_{s}.png">'+EFTType[r][s]+'</a>'+'</br>'
                    pruning_web.write(pstr)
                
            else:
                print(" -",b)
                pruning_web.write(' - {b}</br>')
                
        pruning_web.close()

    if opts.verbose:
        print("TOTALS:",totals)
    ymax=max(max(totals),ymax)

    ymax*=1.1

    if opts.verbose:
        print("YMAX:",ymax)

    xsize=int(len(b_dict)/5)+1
    ysize=max(int((6./16.)*xsize),1)
    
    #fig, ax = plt.subplots(figsize=(int(len(b_dict)/5)+1, len(group)+1))
    fig, ax = plt.subplots(figsize=(xsize,ysize))
    bottom = np.zeros(len(b_dict))

    ind=np.arange(len(bins))
                
    for n,(boolean,p_split) in enumerate(p_split.items()):
        if stack:
            #p = ax.bar(xpos, p_split, width, label=boolean, bottom=bottom)
            p = ax.bar(xpos, p_split, width, label=split[boolean]["title"], bottom=bottom)
            bottom += p_split
        else:

            #p = ax.bar([x+(n*(width/2.)) for x in xpos], p_split, width, label=boolean)
            p = ax.bar([x+(n*(width/2.)) for x in xpos], p_split, width, label=split[boolean]["title"])

    if not stack:
        plt.xticks(xpos, [bin.replace('bin','').replace('_',' ') for bin in bins])
        
    savename="drel"
    title="Relative"
    if useyield:
        savename="dabs"
        title="Absolute"
    if background:
        savename=f"dabs{background}"
        title=""

        if background=="SB":
            title="S/B"
        elif background=="significance":
            title=r"S/$\sqrt{B}$"
        elif background=="significancesyst":
            title=r"S/$\sqrt{B+0.3*B^2}$"

    if stack:
        savename=savename+"_stack"
        ymin=0
    if opts.verbose:
        print("YMIN:",ymin)

    
    if not frac:
        ax.set_title(f"{title} change of {ordername} terms by {splitname} (combine {groupname}) per bin")
        savename=savename+f"_{ordername}_{splitname}_{groupname}"
    else:
        ax.set_title(f"{title} (fractional) change of {ordername} terms by {splitname} (combine {groupname}) per bin")
        savename=savename+f"_frac_{ordername}_{splitname}_{groupname}"
                
    quot = len(split) // max(10,(len(group)*2))
    
    ax.legend(loc='upper right', bbox_to_anchor=(1.06+(0.09*(quot+1)),1.0), ncol=(quot+1))

    if not frac:
        if useyield:
            ax.set_ylim([max(-500,ymin-(abs(0.1*ymin))), min(500.0,ymax)])
        else:
            ax.set_ylim([max(-4.0-(abs(0.1*ymin))), min(4.0,ymax)])
    else:
        ax.set_ylim([0, 1.1])
    plt.xticks(rotation = 90) 

    fig.subplots_adjust(right=min(0.87,1.-(0.075*(quot+1))))
    fig.subplots_adjust(top=0.92,bottom=0.35)

    #fig.tight_layout()


    if suff:
        savename=savename+"_"+suff

    print("Saving figure:",f'{basedir}/EFT/EFTViz/{savename}')
    plt.savefig(f'{basedir}/EFT/EFTViz/{savename}.png')
    plt.savefig(f'{basedir}/EFT/EFTViz/{savename}.pdf')
    #plt.show()




def plot_bar_2D(b_dict,splitname,groupname,insignals,inefts,order=None,stack=True,useyield=False,frac=None,eftlims=True,background=False,suff=None):

    bins = [b for b in b_dict]
    ind=np.arange(len(bins))

    width = 0.8
    if not stack:
        width=0.3

    ## No gaps
    #xpos=ind + width / 2.

    # Insert gaps between regions
    xpos=[]
    icount=0
    old=None
    for b in b_dict:
        new=b.split('_bin')[0]
        #Just need this first time
        if not old:
            old=new

        if new != old:
            old=new
            icount+=1
            xpos.append(icount+width/2)
        else:
            xpos.append(icount+width/2)

        icount+=1

    
    split=None

    ordername="unknown"
    if order==1: ordername="linear"
    if order==2: ordername="quadratic"
    
    
    if splitname=="process":
        split=insignals
        group=inefts
    else:
        split=inefts
        group=insignals
        
    totals_all=np.zeros(len(b_dict))
    totals=[]
    ymin=0
    ymax=-1

    # need array of plot values for split
    p_splits=[]

    for g in group:
        tmptotals=np.zeros(len(b_dict))
        print(g)
        # Now that we are in 2D mode, what would have been the group, is now the separate rows
        p_split = {}

        for s in split:
            if opts.verbose:
                print("==",s)
            b_vals=[]
            
            for b in b_dict:
                if opts.verbose:
                    print("====",b)

                #if "SPACE" in b:
                #    b_vals.append(0)
                #    continue
                
                nb=int(b.split('_bin')[1])
                
                res=0
                if not useyield:
                    # Get the relative difference - i.e. just use the sigma_EFT/sigma_SM values from the EFT parametrisaton        
                    if splitname=="process":
                        eqn=geteqn(b_dict[b],insignals=[s],inefts=[g],inorder=order)
                    else:
                        eqn=geteqn(b_dict[b],inefts=[s],insignals=[g],inorder=order)
                        
                        
                    # Evaluate equation at a given set of coeff values
                    if eftlims:
                        res=eval(eqn,{k:v["limit"] for k,v in inefts.items()})
                    else:
                        res=eval(eqn,{k:1.0 for k,v in inefts.items()})
                    res=res-1.0
                    
                else:
                    # Get the absolute difference - i.e. sigma_EFT/sigma_SM multiplied by the SM yield
                    region=b.split('_bin')[0]
                    hyield=0

                    if splitname=="process":
                        # Still need to get eqn and evaluate at given WC values
                        #eqn=geteqn(b_dict[b],insignals=[s],inorder=order)
                        eqn=geteqn(b_dict[b],insignals=[s],inefts=[g],inorder=order)
                        if eftlims:
                            res=eval(eqn,{k:v["limit"] for k,v in inefts.items()})
                        else:
                            res=eval(eqn,{k:1.0 for k,v in inefts.items()})

                        res=res-1.0

                        # Use TRExFitter histo files to get yields
                        filename=f"{basedir}/Histograms/{runname}_{region}_histos.root"
                        rfile=TFile.Open(filename)
                        histname=f"{region}/SM_{s}_{b}/nominal/{region}_SM_{s}_{b}"
                        hist=rfile.Get(histname)
        
                        if hist:
                            hyield=hist.Integral()
                        else:
                            print(f"WARNING: {histname} not found in {filename}")
                            
                        #print(filename,histname,hyield,hyield*res)

                        
                        # Result is yeild times relative EFT variation
                        res=hyield*res

                        if background:
                            sumbkg=0
                            if "bkg" in b_dict[b]:
                                sumbkg=b_dict[b]["bkg"]
                            else:
                                print("Finding background for",b)
                                for bkg in bkgs:
                                
                                    histname=f"{region}/{bkg}/nominal/{region}_{bkg}"
                                    hist=rfile.Get(histname)
                                    if hist:
                                        hyield=hist.GetBinContent(nb+1)
                                        sumbkg+=hyield
                                        if opts.verbose:
                                            print("BKG:",bkg,nb+1,hyield,sumbkg)
                                    else:
                                        print(f"WARNING: {histname} not found in {filename}")
                                
                                for signal in signals:
                                
                                    histname=f"{region}/{signal}/nominal/{region}_{signal}"
                                    hist=rfile.Get(histname)
                                    if hist:
                                        hyield=hist.GetBinContent(nb+1)
                                        sumbkg+=hyield
                                        if opts.verbose:
                                            print("BKG (SM):",signal,nb+1,hyield,sumbkg)
                                    else:
                                        print(f"WARNING: {histname} not found in {filename}")

                                b_dict[b]["bkg"]=sumbkg

                            if opts.verbose:
                                print("BKG TOTAL:",sumbkg)

                                    
                            if background=="SB":
                                res=res/sumbkg
                                if opts.verbose:
                                    print("S/B:",res)

                            elif background=="significance":
                                res=res/math.sqrt(sumbkg)
                                if opts.verbose:
                                    print("S/sqrt(B):",res)

                            elif background=="significancesyst":
                                res=res/math.sqrt(sumbkg+(0.3*sumbkg*0.3*sumbkg))
                                if opts.verbose:
                                    print("S/sqrt(B+0.3B^2:",red)

                    
                    else:
                        #if split is by EFT need to add up all the signal yields by hand
                        filename=f"{basedir}/Histograms/{runname}_{region}_histos.root"
                        rfile=TFile.Open(filename)
                        for signal in signals:
                            if signal!=g:continue
                            eqn=geteqn(b_dict[b],insignals=[signal],inefts=[s],inorder=order)
                            if eftlims:
                                signalres=eval(eqn,{k:v["limit"] for k,v in inefts.items()})
                            else:
                                signalres=eval(eqn,{k:1.0 for k,v in inefts.items()})
                            #signalres=eval(eqn,{k:v for k,v in inefts.items()})
                            signalres=signalres-1.0
        
                            histname=f"{region}/SM_{signal}_{b}/nominal/{region}_SM_{signal}_{b}"
                            hist=rfile.Get(histname)
                            if hist:
                                hyield=hist.Integral()
                            else:
                                print(f"WARNING: {histname} not found in {filename}")

                            if opts.verbose:
                                print(filename,histname,hyield,hyield*signalres,res)
        
                            res+=hyield*signalres    
        
                        if background:
                            sumbkg=0
                            if "bkg" in b_dict[b]:
                                sumbkg=b_dict[b]["bkg"]
                            else:
                                print("Finding background for",b)
                                for bkg in bkgs:
                                
                                    histname=f"{region}/{bkg}/nominal/{region}_{bkg}"
                                    hist=rfile.Get(histname)
                                    if hist:
                                        hyield=hist.GetBinContent(nb+1)
                                        sumbkg+=hyield
                                        if opts.verbose:
                                            print("BKG:",bkg,nb+1,hyield,sumbkg)
                                    else:
                                        print(f"WARNING: {histname} not found in {filename}")
                                
                                
                                for signal in signals:
                                
                                    histname=f"{region}/{signal}/nominal/{region}_{signal}"
                                    hist=rfile.Get(histname)
                                    if hist:
                                        hyield=hist.GetBinContent(nb+1)
                                        sumbkg+=hyield
                                        if opts.verbose:
                                            print("BKG (SM):",signal,nb+1,hyield,sumbkg)
                                    else:
                                        print(f"WARNING: {histname} not found in {filename}")

                                b_dict[b]["bkg"]=sumbkg

                            if opts.verbose:
                                print("BKG TOTAL:",sumbkg)

                                    
                            if background=="SB":
                                res=res/sumbkg
                                if opts.verbose:
                                    print("S/B:",res)

                            elif background=="significance":
                                res=res/math.sqrt(sumbkg)
                                if opts.verbose:
                                    print("S/sqrt(B):",res)

                            elif background=="significancesyst":
                                res=res/math.sqrt(sumbkg+(0.3*sumbkg*0.3*sumbkg))
                                if opts.verbose:
                                    print("S/sqrt(B+0.3B^2:",red)

                            
                        
                b_vals.append(res)

            if frac:
                b_vals=[abs(b) for b in b_vals]
                
            #print(b_vals)
            ymin=min(ymin,min(b_vals))
            ymax=max(ymax,max(b_vals))
            tmptotals+=np.array(b_vals)
            totals_all+=np.array(b_vals)
            p_split[s]=np.array(b_vals)

        totals.append(tmptotals)
        p_splits.append(p_split)
        
    if frac:
        # If in fraction mode, divide out all the values by the total for that bin
        for i,g in enumerate(group):
            for s in split:
                #this would be frac wrt all
                #p_splits[i][s]=np.divide(p_splits[i][s],np.array(totals_all))

                #p_splits[i][s]=np.divide(p_splits[i][s],np.array(totals[i]))
                a=p_splits[i][s]
                b=np.array(totals[i])
                p_splits[i][s]=np.divide(a,b, out=np.zeros_like(a), where=b!=0)


    
    if opts.verbose:
        print("TOTALS:",totals_all)


    ##ymax=max(totals_all)*1.1
    #ymax=-1
    #for i,g in enumerate(group):
    #    if max(totals[i]) > ymax: ymax=max(totals[i])
    ymax*=1.1
        
    if opts.verbose:
        print("YMAX:",ymax)


    xsize=int(len(b_dict)/5)+1
    ybuffer=0
    ysize=max(3,int(1.2*len(group)))

    nrows=len(group)
    fig, ax = plt.subplots(nrows,1,figsize=(xsize, ysize))
    


    #items=p_split.items()


    for i,g in enumerate(group):

        thisax=None
        if len(group)>1:
            thisax=ax[i]
        else:
            thisax=ax
        
        #plt.subplot(4,1,i+1)
        bottom = np.zeros(len(b_dict))
        for n,(boolean,p_splits[i]) in enumerate(p_splits[i].items()):
            if stack:
                #p = thisax.bar(bins, p_splits[i], width, label=boolean, bottom=bottom)
                p = thisax.bar(xpos, p_splits[i], width, label=split[boolean]["title"], bottom=bottom)
                bottom += p_splits[i]
                test="something"

                
            else:
                test="somethingelse"
                #p = thisax.bar(ind+(n*(width/2.)), p_splits[i], width, label=boolean)
                p = thisax.bar([x+(n*(width/2.)) for x in xpos], p_splits[i], width, label=split[boolean]["title"])
        
        thisax.set_ylabel(group[g]["title"],labelpad=1,loc="top",rotation=0)
        thisax.yaxis.set_label_coords(-0.07, 0.5)   
        if not frac:
            if useyield:
                thisax.set_ylim([max(-500,ymin-(abs(0.1*ymin))), min(500.0,ymax)])
            else:
                thisax.set_ylim([max(-1.5,ymin-(abs(0.1*ymin))), min(1.5,ymax)])
        else:
            thisax.set_ylim([-0.1, 1.1])

        if i < len(group)-1:
            thisax.tick_params(
                axis='x',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                bottom=False,      # ticks along the bottom edge are off
                top=False,         # ticks along the top edge are off
                labelbottom=False) # labels along the bottom edge are off
            


    plt.xticks(xpos, [bin.replace('bin','').replace('_',' ') for bin in bins])
    plt.subplots_adjust(wspace=0, hspace=0)

    #fig.align_ylabels()

    savename="2D_drel"
    title="Relative"
    if useyield:
        savename="2D_dabs"
        title="Absolute"
    if background:
        savename=f"2D_dabs{background}"
        title=""
        if background=="SB":
            title="S/B"
        elif background=="significance":
            title=r"S/$\sqrt{B}$"
        elif background=="significancesyst":
            title=r"S/$\sqrt{B+0.3*B^2}$"


        
    if stack:
        savename=savename+"_stack"
        ymin=0

    if opts.verbose:
        print("YMIN:",ymin)


    baseax=None
    if len(group)>1:
        baseax=ax[0]
    else:
        baseax=ax
    
    if not frac:
        baseax.set_title(f"{title} change of {ordername} terms by {splitname} per bin")
        savename=savename+f"_yield_{ordername}_{splitname}_{groupname}"
    else:
        baseax.set_title(f"{title} (fractional) change of {ordername} terms by {splitname} per bin")
        savename=savename+f"_frac_{ordername}_{splitname}_{groupname}"
                
    #baseax.legend(loc="upper right", bbox_to_anchor=(1.1, 0.9))
    quot = len(split) // max(10,len(group)*2)
    baseax.legend(loc='upper right', bbox_to_anchor=(1.03+(0.1*(quot+1)),1.0), ncol=(quot+1))

    plt.xticks(rotation = 90) 
    #fig.subplots_adjust(right=0.87)
    fig.subplots_adjust(right=min(0.87,1.-(0.08*(quot+1))))
        
    #fracbottom=max(0.2,1.0/float(len(group)+1))

    #margin=frac*height
    #good = 0.15 * 12 = ~2
    #frac=2./height

    fracbottom=min(0.5,2./ysize)
    
    fig.subplots_adjust(bottom=fracbottom)
    fig.subplots_adjust(top=0.92)
    
    if eftlims:
        savename=savename+"_eftlims"

    if suff:
        savename=savename+"_"+suff

    print("Saving figure:",f'{basedir}/EFT/EFTViz/{savename}')
    plt.savefig(f'{basedir}/EFT/EFTViz/{savename}.png')
    plt.savefig(f'{basedir}/EFT/EFTViz/{savename}.pdf')
    #plt.show()



def plot_heatmap(b_dict,insignals,inefts,order=None,useyield=False,frac=None,eftlims=False,background=False):

    bins = [b for b in b_dict]


    p_split = {}
    p_split_rel = {}
    split=None

    ordername="unknown"
    if order==1: ordername="linear"
    if order==2: ordername="quadratic"
    

    
    split=inefts
    group=insignals
        
    totals=np.zeros(len(b_dict))
    ymin=0
    ymax=-1

    ## lists for heat maps
    df_op = []
    df_reg = []
    df_bin = []
    df_sig = []

    EFTType={}
    # Loop over split values
    for s in split:
        print("Split:",s)
        b_vals=[]
        b_vals_rel=[]

        # Loop over region bins
        for b in b_dict:
            if opts.verbose:
                print(b)

            nb=int(b.split('_bin')[1])
            
            res=0
            rel=0
            
            if not useyield:
                # Get the relative difference - i.e. just use the sigma_EFT/sigma_SM values from the EFT parametrisaton
                
                
                
                eqn=geteqn(b_dict[b],inefts=[s],inorder=order)
                    
                # Evaluate equation at a given set of coeff values
                #res=eval(eqn,{k:v["limit"] for k,v in inefts.items()})
                #@todo Should really check largest correction for positive and negative...?!
                if eftlims:
                    res=eval(eqn,{k:v["limit"] for k,v in inefts.items()})
                else:
                    res=eval(eqn,{k:1.0 for k,v in inefts.items()})
                

                res=res-1.0
                rel=res
                
            else:
                # Get the absolute difference - i.e. sigma_EFT/sigma_SM multiplied by the SM yield
                
                region=b.split('_bin')[0]
                hyield=0
                
                #if split is by EFT need to add up all the signal yields by hand
                filename=f"{basedir}/Histograms/{runname}_{region}_histos.root"
                rfile=TFile.Open(filename)
                sig_tot=0.
                for signal in signals:
                    eqn=geteqn(b_dict[b],insignals=[signal],inefts=[s],inorder=order)
                    #print("TEST",eqn,)
                    signalres=eval(eqn,{k:v["limit"] for k,v in inefts.items()})
                    signalres=signalres-1.0

                    
                    histname=f"{region}/SM_{signal}_{b}/nominal/{region}_SM_{signal}_{b}"
                    hist=rfile.Get(histname)
                    if hist:
                        hyield=hist.Integral()
                    else:
                        print(f"WARNING: {histname} not found in {filename}")
                    if opts.verbose:
                        print(filename,histname,hyield,hyield*signalres,res)

                    sig_tot+=hyield
                    res+=hyield*signalres    

                rel=res/sig_tot if sig_tot else 0.
                
                if background:
                    sumbkg=0
                    if "bkg" in b_dict[b]:
                        sumbkg=b_dict[b]["bkg"]
                    else:
                        print("Finding background for",b)
                        for bkg in bkgs:
                        
                            histname=f"{region}/{bkg}/nominal/{region}_{bkg}"
                            hist=rfile.Get(histname)
                            if hist:
                                hyield=hist.GetBinContent(nb+1)
                                sumbkg+=hyield
                                if opts.verbose:
                                    print("BKG:",bkg,nb+1,hyield,sumbkg)
                            else:
                                print(f"WARNING: {histname} not found in {filename}")
                        
                        for signal in signals:
                        
                            histname=f"{region}/{signal}/nominal/{region}_{signal}"
                            hist=rfile.Get(histname)
                            if hist:
                                hyield=hist.GetBinContent(nb+1)
                                sumbkg+=hyield
                                if opts.verbose:
                                    print("BKG (SM):",signal,nb+1,hyield,sumbkg)
                            else:
                                print(f"WARNING: {histname} not found in {filename}")

                        b_dict[b]["bkg"]=sumbkg
                
                    if background=="SB":
                        res=res/sumbkg
                    elif background=="significance":
                        res=res/math.sqrt(sumbkg)
                    elif background=="significancesyst":
                        res=res/math.sqrt(sumbkg+(0.3*sumbkg*0.3*sumbkg))

                           
                            
            b_vals.append(res)
            b_vals_rel.append(rel)
            
        if opts.verbose:
            print(b_vals)
        ymin=min(ymin,min(b_vals))
        ymax=max(ymax,max(b_vals))
        totals+=np.array(b_vals)
        p_split[s]=np.array(b_vals)
        p_split_rel[s]=np.array(b_vals_rel)

    if frac:
        for s in split:
            p_split[s]=np.divide(p_split[s],np.array(totals))

    # Do pruning
    if background=="significance":
        pruned=[]
        #reset
        for b in b_dict:
            b_dict[b]["significant"]=[]
        
        threshold=0.5

        
        for s in split:
            maxval=-999
            # find regions for energy growth test fit
            regions=[]
            
            for ib,b in enumerate(b_dict):
                region=b.split('_bin')[0]
                if region not in regions:
                    regions.append(region)
                    
                val=p_split[s][ib]
                #print(s,b,val)
                if abs(val)>threshold:
                    if "significant" in b_dict[b]:
                        b_dict[b]["significant"].append((s,val))
                    else:
                        b_dict[b]["significant"]=[(s,val)]
                        
                maxval=max(abs(val),maxval)
            if maxval < threshold:
                pruned.append(s)
                print(f'  --> Max val = {maxval}, {s} is pruned')
            else:
                # Test energy growth
                for r in regions:
                    x=[]
                    y=[]

                    for ib,b in enumerate(b_dict):
                        #print("DEBUG:",f'{r}_bin', b)
                        
                        if f'{r}_bin' in b:
                            nbin=b.split(f'{r}_bin')[1]
                            x.append(float(nbin))
                            vals=[x[1] for x in b_dict[b]["significant"] if x[0]==s]
                            if len(vals)>1:
                                print("ERROR: Too many operator values found!")
                            y.append(float(p_split_rel[s][ib]))
                            continue
             
        ### create heatmaps and summary tables ### 
        for s in split:
            regions=[]
            for ib,b in enumerate(b_dict):
                region=b.split('_bin')[0]
                if region not in regions:
                    regions.append(region)
                                
                val=p_split[s][ib]
                # if "SR" in region: 
                df_op.append(s)
                df_reg.append(region)
                df_bin.append(b)
                df_sig.append(val)

        df = pd.DataFrame({'Operator': df_op, 'Bin': df_bin, 'Significance': df_sig})
        pivoted_df = pd.pivot_table(df, values='Significance', 
                                index='Operator', 
                                columns='Bin',
                                sort=False)
        print("Saving file:",f'{basedir}/EFT/EFTViz/Heatmaps/all_operator_significance_in_bins.csv')
        pivoted_df.to_csv(f'{basedir}/EFT/EFTViz/SummaryTables/all_operator_significance_in_bins.csv')

        ### Filter out operators that have a significance of less than 0.5 everywhere
        condition = (abs(pivoted_df) < 0.5).all(axis=1)
        filtered_df = pivoted_df[~condition]        
        print("Saving file:",f'{basedir}/EFT/EFTViz/Heatmaps/pruned_operator_significance_in_bins.csv')
        filtered_df.to_csv(f'{basedir}/EFT/EFTViz/SummaryTables/pruned_operator_significance_in_bins.csv')
        
        # row_condition = (abs(pivoted_df) < 0.5).all(axis=1)
        # filtered_df_inter = pivoted_df[~row_condition]
        # column_condition = (filtered_df_inter.abs() < 0.5).all(axis=0)
        # filtered_df = filtered_df_inter.loc[:, ~column_condition]
        # print("Saving file:",f'{basedir}/EFT/EFTViz/Heatmaps/pruned_operator_significance_in_bins.csv')
        # filtered_df.to_csv(f'{basedir}/EFT/EFTViz/SummaryTables/pruned_operator_significance_in_bins.csv')
        
        ##Maximum significance for plotting
        maximum_sig = abs(pivoted_df.to_numpy()).max()
        
        ### HEATMAP WITHOUT PRUNING
        fig,ax = plt.subplots(figsize=(30,10))
        im = ax.imshow(pivoted_df, vmin=-10, vmax=10, cmap='seismic', aspect='auto')
        ax.set_xticks( np.arange( len( list(pivoted_df) ) ), labels=list(pivoted_df)) 
        ax.set_yticks(np.arange( len ( pivoted_df.index.to_list() )), labels=pivoted_df.index.to_list()) 
        
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor")
        for i in range( len( list(pivoted_df) ) ):
            for j in range( len ( pivoted_df.index.to_list() ) ):
                value = pivoted_df.iloc[j, i]
                text = ax.text(i, j, f'{value:.1f}',  # Round to one decimal place
                       ha="center", va="center", color="k")
        ax.set_title(r"S/$\sqrt{B}$")
        fig.tight_layout(pad=0.8)
        print("Saving figure:",f'{basedir}/EFT/EFTViz/Heatmaps/Heatmap_all_operators')
        plt.savefig(f'{basedir}/EFT/EFTViz/Heatmaps/Heatmap_all_operators.png')
        plt.savefig(f'{basedir}/EFT/EFTViz/Heatmaps/Heatmap_all_operators.pdf')

        ### HEATMAP WITH PRUNING
        fig,ax = plt.subplots(figsize=(30,10))
        im = ax.imshow(filtered_df, vmin=-10, vmax=10, cmap='seismic', aspect='auto')
        ax.set_xticks( np.arange( len( list(filtered_df) ) ), labels=list(filtered_df)) 
        ax.set_yticks(np.arange( len ( filtered_df.index.to_list() )), labels=filtered_df.index.to_list()) 

        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor")
        for i in range( len( list(filtered_df) ) ):
            for j in range( len ( filtered_df.index.to_list() ) ):
                value = filtered_df.iloc[j, i]
                text = ax.text(i, j, f'{value:.1f}',  # Round to one decimal place
                       ha="center", va="center", color="k")
        ax.set_title(r"S/$\sqrt{B}$")
        fig.tight_layout(pad=0.8)
        print("Saving figure:",f'{basedir}/EFT/EFTViz/Heatmaps/Heatmap_pruned_operators')
        plt.savefig(f'{basedir}/EFT/EFTViz/Heatmaps/Heatmap_pruned_operators.png')
        plt.savefig(f'{basedir}/EFT/EFTViz/Heatmaps/Heatmap_pruned_operators.pdf')
        ###


def geteqn(binobj,insignals=None,inefts=None,inorder=None):
    mysignals=insignals if insignals else signals
    myefts=inefts if inefts else [eft for eft in efts]
    eqn="1.0"
    #print(mysignals,myefts)
    
    for s in binobj:
        #print("SIGNAL:",p)
        if s not in mysignals: continue
        #print("SIGNAL:",p)

        for t in binobj[s]:
            #print("TERM:",t)

            testeft=None

            for e in myefts:
                order=binobj[s][t]["orders"][list(efts.keys()).index(e)]
                sumorders=sum(binobj[s][t]["orders"])
                #print(binobj[s][t]["orders"],sumorders,list(efts.keys()).index(e),order)
                if order:
                    if not inorder or sumorders <= inorder:
                        testeft=True
                        break

            if not testeft: continue

            
            eqn=eqn+"+"+binobj[s][t]['coeff']+"*"+t

    return eqn


if __name__ == '__main__':
    main()
