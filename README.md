# EFTViz

Python script to extract EFT sensitivity from (EFT)TRExFitter outputs and make nice visualisations of them.

| Relative EFT dependence | Fractional absolute EFT dependence |
|--------|--------|
|![relative EFT dependence](2D_drel_yield_linear_EFT_process_eftlims.png) | ![fractional absolute EFT dependence](2D_dabs_stack_frac_quadratic_process_EFT_eftlims.png)|

Signal/sqrt(B) change of linear terms for EFT operators in all regions:
![Heatmap example](Heatmap_all_operators.png)